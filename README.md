# Шаблон для верстки.

JS: jQuery, VueJS

CSS: Sass

Сборщик: Gulp

Подключена сетка Bootstrap


### Набор подключенных плагинов и библиотек

- axios.js (ajax-запросы);
- vuelidate' (валидация форм на Vue);
- PageScroll2id;
- slick (слайдер);
- bodyScrollLock (отключает скролл body);
- csshake (тряска элементов на css);
- fancybox 3 (попапы и галереи);
- typed.js (эффект печати текста на js);
- swiper (маленький удобненький слайдер http://idangero.us/swiper/get-started/).

##### fancybox 3

https://fancyapps.com/fancybox/3/

https://github.com/fancyapps/fancybox
