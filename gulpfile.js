let
  gulp          = require('gulp'),
  sass          = require('gulp-sass'),
  autoprefixer  = require('gulp-autoprefixer'),
  sourcemaps    = require('gulp-sourcemaps'),
  clean         = require('gulp-clean'),
  notify        = require('gulp-notify'),
  plumber       = require('gulp-plumber'),
  concat        = require('gulp-concat'),
  bs            = require('browser-sync').create(),
  connectPHP    = require('gulp-connect-php'),
  inject        = require('gulp-inject-string');

let autoPrefixerSettings = [
  'Chrome >= 29',
  'Firefox >= 28',
  'Safari >= 9',
  'Opera >= 17',
  'Edge >= 12',
  'last 5 version',
  'not IE <= 11'
];

let paths = {
  // пути к исходникам библиотек, размещать в той очередности, которая требуется при подключении файлов в проект
  'libsSrc': {
    'js' : {
      'dev': [
        'node_modules/jquery/dist/jquery.js',
        'node_modules/vue/dist/vue.js',
        // 'node_modules/axios/dist/axios.js',
        'node_modules/vuelidate/dist/vuelidate.min.js',
        'node_modules/vuelidate/dist/validators.min.js',
        'node_modules/page-scroll-to-id/jquery.malihu.PageScroll2id.js',
        'node_modules/slick-carousel/slick/slick.js',
        'node_modules/body-scroll-lock/lib/bodyScrollLock.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
        //'node_modules/typed.js/lib/typed.js'
      ],
      'prod': [
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/vue/dist/vue.min.js',
        // 'node_modules/axios/dist/axios.min.js',
        'node_modules/vuelidate/dist/vuelidate.min.js',
        'node_modules/vuelidate/dist/validators.min.js',
        'node_modules/page-scroll-to-id/jquery.malihu.PageScroll2id.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'node_modules/body-scroll-lock/lib/bodyScrollLock.min.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
        //'node_modules/typed.js/lib/typed.min.js'
      ]
    },
    'css' : [
      'node_modules/slick-carousel/slick/slick.scss',
      'node_modules/slick-carousel/slick/slick-theme.scss',
      'node_modules/csshake/dist/csshake-little.css'
    ],
    'bootstrapGrid' : 'node_modules/bootstrap-4-grid/scss/**/*'
  },
  'build' : {
    'root' : 'build/',
    'js' : 'build/js/',
    'css' : 'build/css/',
    'images' : 'build/assets/'
  },
  'src' : {
    'root' : 'src/',
    'images' : 'src/assets/',
    'js' : 'src/js/',
    'sass' : {
      'root' : 'src/sass/',
      'libs' : 'src/sass/libs/',
      'common' : 'src/sass/common/',
      'blocks' : 'src/sass/blocks/'
    }
  }
};

let settings = {
  'filenames' : {
    'libsJsFilename' : 'libs.js',
    'mainJsFilename' : 'main.js'
  },
  'addBootstrapAdaptiveMixins' : true // настройка при создании нового блока добавляет в файл строчки с миксинами для адаптивной верстки
};


//// Сервер, брааузерсинк и пр. служебные таски, START ////

// подружить gulp с php: https://threenine.co.uk/php-server-browsersync-gulp/
gulp.task('php', function () {
  connectPHP.server({base: paths.build.root, port: 8010, keepalive: true});
});

gulp.task('bs', ['php'], function () {
  bs.init({
    baseDir: paths.build.root,
    port: 3000, // 3000 - дефолтный, можно и без этого
    open: false, // автооткрытие браузера при запуске таска
    notify: false, // не очень полезная нотификация в правом верхнем углу браузера
    proxy: "localhost:8010"
  });
});

gulp.task('cleanBuild', function () {
  return gulp.src([paths.build.js + '**/*', paths.build.css + '**/*', paths.build.images + '**/*',], {read: false})
  .pipe(clean());
});

//// Сервер, брааузерсинк и пр., START ////


//// Все что касается JS, START ////

gulp.task('jsLibs:dev', function() {
  return gulp.src(paths.libsSrc.js.dev)
  .pipe(concat(settings.filenames.libsJsFilename))
  .pipe(gulp.dest(paths.build.js));
});

gulp.task('jsLibs:prod', function() {
  return gulp.src(paths.libsSrc.js.prod)
  .pipe(concat(settings.libsJsFilename))
  .pipe(gulp.dest(paths.build.js));
});

gulp.task('js', function () {
  return gulp.src(paths.src.js + settings.filenames.mainJsFilename)
  .pipe(gulp.dest(paths.build.js));
});

//// Все что касается JS, END ////


//// Все что касается CSS, SASS и подобного, START ////

gulp.task('cssBootstrap', function () {
  return gulp.src(paths.libsSrc.bootstrapGrid)
  .pipe(gulp.dest(paths.src.sass.libs + 'bootstrap-grid/'))
});

gulp.task('cssLibs', ['cssBootstrap'], function () {
  console.log('Необходимо навести порядок в src: добавить к файлам "_", переименовать .css в .sass');
  return gulp.src(paths.libsSrc.css)
  .pipe(gulp.dest(paths.src.sass.libs))
});

gulp.task('sass', function () {
  return gulp.src(paths.src.sass.root + '*.{scss,sass}')
  .pipe(plumber({
    errorHandler: notify.onError(function (err) {
      return {
        title: "sass error",
        message: err.message
      };
    })
  }))
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(autoprefixer(autoPrefixerSettings, {cascade: true}))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(paths.build.css));
});

// таск добавляет запись в _blocks.sass и добавляет строку с наименованием класса
// в созданный файл при добавлении нового sass-файла в папку blocks
gulp.task('newSassBlockWatcher', function () {
  gulp.watch([paths.src.sass.blocks + '*.sass']).on('change', function (file) {
    let fileData = file;
    let actionType = fileData['type'];
    if (actionType === 'added') {
      let filePath = fileData['path'];
      let fileName = filePath.split('/');
      fileName = fileName[fileName.length - 1];

      let sassBlockString = ' \n@import "blocks/' + fileName;

      gulp.src(paths.src.sass.root + '_blocks.sass')
      .pipe(inject.append(sassBlockString))
      .pipe(gulp.dest(paths.src.sass.root));

      let className = fileName.split('.')[0];
      className = className.substr(1);

      let injectStrings = '.' + className + ' // added automatically by Gulp \n';

      if(settings.addBootstrapAdaptiveMixins) {
        injectStrings =
          injectStrings +
          '\n' +
          '\n' +
          '\n' +
          '@include media-breakpoint-down(xl)' +
          '\n' +
          '\n' +
          '\n' +
          '@include media-breakpoint-down(lg)' +
          '\n' +
          '\n' +
          '\n' +
          '@include media-breakpoint-down(md)' +
          '\n' +
          '\n' +
          '\n' +
          '@include media-breakpoint-down(sm)' +
          '\n' +
          '\n' +
          '\n' +
          '@include media-breakpoint-down(xs)' +
          '\n';
      }

      gulp.src(paths.src.sass.blocks + fileName)
      .pipe(inject.append(injectStrings))
      .pipe((gulp.dest(paths.src.sass.blocks)));
      console.log('File ' + fileName + ' added into blocks; classname added to ' + fileName);
    }
  })
});

//// Все что касается CSS, SASS и подобного, END ////


//// Все что касается обработки изображений и подобного, START ////

gulp.task('assets', function () {
  return gulp.src(paths.src.images + '**/*')
  .pipe(gulp.dest(paths.build.images));
});

//// Все что касается обработки изображений и подобного, END ////


//// watch, default START ////

gulp.task('watch', ['sass', 'js', 'assets', 'newSassBlockWatcher', 'bs'], function () {
  gulp.watch(paths.src.sass.root + '**/*.{scss,sass}', ['sass']);
  gulp.watch(paths.src.js + '**/*', ['js']);
  gulp.watch(paths.src.images + '**/*', ['assets']);
  gulp.watch(paths.build.root + '**/*', bs.reload);
});

gulp.task('default', ['watch']);

//// watch, default END ////

