// Настройки

let debugStatus = true;


// Глобальные функции

function debug(data) {
  if (debugStatus) {
    console.log(data);
  }
}


// Глобальные переменные

let preloaderIsActive = true;



// START Vue code

// END Vue code


// START jQuery code
(function(){

  // START Отладочные функции
  if (debugStatus) {

  }
  // END Отладочные функции

  // START Функции после полной загрузки DOM-дерева
  $(document).ready(function() {

    // START Функции на скролл
    $(window).on('scroll', function () {


    });
    // END Функции на скролл

    // START Функции на ресайз
    $(window).on('resize', function () {


    });
    // END Функции на ресайз

  });
  // END Функции после полной загрузки DOM-дерева


  // START Функции после полной загрузки документа
  $(window).on('load', function () {


  });
  // END Функции после полной загрузки документа

})();
// END jQuery code
